## Frontend Test

## Description

You have to guess a word and tell the user the number of similar letters in the words. For example, the word  
"moon" is the word we want. If the user enters the word "maka", notify the user that 1 similar letter has been found in the original word

*   Use this technology: Next.js, Typescript, Tailwindcss
*   You can change the sample design ([Sample Design](https://iili.io/HokRVsV.png) )
*   After guessing the secret word, the user can not enter any other word and show the appropriate message to the user.
